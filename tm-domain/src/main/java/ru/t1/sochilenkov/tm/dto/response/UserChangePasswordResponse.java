package ru.t1.sochilenkov.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.sochilenkov.tm.model.User;

@NoArgsConstructor
public final class UserChangePasswordResponse extends AbstractUserResponse {

    public UserChangePasswordResponse(@NotNull final User user) {
        super(user);
    }

}
