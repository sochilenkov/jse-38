package ru.t1.sochilenkov.tm.exception.system;

public class AccessDeniedException extends AbstractSystemException {

    public AccessDeniedException() {
        super("Error! Access denied...");
    }

}
