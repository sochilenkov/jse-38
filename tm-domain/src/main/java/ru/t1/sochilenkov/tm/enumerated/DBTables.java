package ru.t1.sochilenkov.tm.enumerated;

import org.jetbrains.annotations.NotNull;

public enum DBTables {

    TM_PROJECT,
    TM_TASK,
    TM_USER,
    TM_SESSION

}
