package ru.t1.sochilenkov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sochilenkov.tm.api.repository.IUserRepository;
import ru.t1.sochilenkov.tm.enumerated.DBColumns;
import ru.t1.sochilenkov.tm.enumerated.DBTables;
import ru.t1.sochilenkov.tm.enumerated.Role;
import ru.t1.sochilenkov.tm.model.User;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    @NotNull
    protected String getTableName() {
        return DBTables.TM_USER.name();
    }

    @Override
    @NotNull
    @SneakyThrows
    public User fetch(@NotNull final ResultSet row) {
        @NotNull final User user = new User();
        user.setId(row.getString(DBColumns.ID_COLUMN.getColumnName()));
        user.setLogin(row.getString(DBColumns.LOGIN_COLUMN.getColumnName()));
        user.setPasswordHash(row.getString((DBColumns.PASSWORD_COLUMN.getColumnName())));
        user.setEmail(row.getString((DBColumns.EMAIL_COLUMN.getColumnName())));
        user.setFirstName(row.getString((DBColumns.FIRST_NAME_COLUMN.getColumnName())));
        user.setLastName(row.getString((DBColumns.LAST_NAME_COLUMN.getColumnName())));
        user.setMiddleName(row.getString((DBColumns.MIDDLE_NAME_COLUMN.getColumnName())));
        user.setLocked(row.getBoolean((DBColumns.LOCKED_COLUMN.getColumnName())));
        user.setRole(Role.valueOf(row.getString(DBColumns.ROLE_COLUMN.getColumnName())));
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User add(@NotNull final User model) {
        @NotNull final String query = String.format(
                "INSERT INTO %s (%s, %s, %s, %s, %s, %s, %s, %s, %s) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
                DBTables.TM_USER.name(), DBColumns.ID_COLUMN.getColumnName(),
                DBColumns.LOGIN_COLUMN.getColumnName(), DBColumns.PASSWORD_COLUMN.getColumnName(),
                DBColumns.EMAIL_COLUMN.getColumnName(), DBColumns.FIRST_NAME_COLUMN.getColumnName(),
                DBColumns.LAST_NAME_COLUMN.getColumnName(), DBColumns.MIDDLE_NAME_COLUMN.getColumnName(),
                DBColumns.LOCKED_COLUMN.getColumnName(), DBColumns.ROLE_COLUMN.getColumnName()
        );
        try (@NotNull PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, model.getId());
            statement.setString(2, model.getLogin());
            statement.setString(3, model.getPasswordHash());
            statement.setString(4, model.getEmail());
            statement.setString(5, model.getFirstName());
            statement.setString(6, model.getLastName());
            statement.setString(7, model.getMiddleName());
            statement.setBoolean(8, model.getLocked());
            statement.setString(9, model.getRole().name());
            statement.executeUpdate();
        }
        return model;
    }
    
    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@NotNull final String login) {
        @NotNull final String query = String.format(
                "SELECT * FROM %s WHERE %s = ? LIMIT 1",
                getTableName(), DBColumns.LOGIN_COLUMN.getColumnName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, login);
            String schema = connection.getSchema();
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@NotNull final String email) {
        @NotNull final String query = String.format(
                "SELECT * FROM %s WHERE %s = ? LIMIT 1",
                getTableName(), DBColumns.EMAIL_COLUMN.getColumnName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, email);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Boolean isLoginExist(@NotNull final String login) {
        @NotNull final String query = String.format(
                "SELECT COUNT(ID) FROM %s WHERE %s = ?",
                getTableName(), DBColumns.LOGIN_COLUMN.getColumnName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, login);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            return resultSet.getInt("COUNT") > 0;
        }
    }


    @NotNull
    @Override
    @SneakyThrows
    public Boolean isEmailExist(@NotNull final String email) {
        @NotNull final String query = String.format(
                "SELECT COUNT(ID) FROM %s WHERE %s = ?",
                getTableName(), DBColumns.EMAIL_COLUMN.getColumnName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, email);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            return resultSet.getInt("COUNT") > 0;
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User update(@NotNull User model) {
        @NotNull final String query = String.format(
                "UPDATE %s SET %s = ?, %s = ?, %s = ?, %s = ?, %s = ?, %s = ?, %s = ? WHERE %s = ?",
                getTableName(), DBColumns.PASSWORD_COLUMN.getColumnName(),
                DBColumns.ROLE_COLUMN.getColumnName(), DBColumns.EMAIL_COLUMN.getColumnName(),
                DBColumns.FIRST_NAME_COLUMN.getColumnName(), DBColumns.LAST_NAME_COLUMN.getColumnName(),
                DBColumns.MIDDLE_NAME_COLUMN.getColumnName(), DBColumns.LOCKED_COLUMN.getColumnName(),
                DBColumns.ID_COLUMN.getColumnName());
        try (@NotNull PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, model.getPasswordHash());
            statement.setString(2, model.getRole().name());
            statement.setString(3, model.getEmail());
            statement.setString(4, model.getFirstName());
            statement.setString(5, model.getLastName());
            statement.setString(6, model.getMiddleName());
            statement.setBoolean(7, model.getLocked());
            statement.setString(8, model.getId());
            statement.executeUpdate();
        }
        return model;
    }

}
