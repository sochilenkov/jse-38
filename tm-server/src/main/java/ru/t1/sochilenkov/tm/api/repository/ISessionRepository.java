package ru.t1.sochilenkov.tm.api.repository;

import ru.t1.sochilenkov.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {
}
