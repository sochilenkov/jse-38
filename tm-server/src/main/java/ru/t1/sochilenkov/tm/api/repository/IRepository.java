package ru.t1.sochilenkov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sochilenkov.tm.model.AbstractModel;

import java.sql.Connection;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public interface IRepository<M extends AbstractModel> {

    void setRepositoryConnection(Connection connection);

    interface IRepositoryOptional<M extends AbstractModel> {

        @NotNull
        Optional<M> findOneById(@NotNull String id) throws Exception;

        @NotNull
        Optional<M> findOneByIndex(@NotNull Integer index) throws Exception;

    }

    default IRepositoryOptional<M> optional() {
        return new IRepositoryOptional<M>() {
            @NotNull
            @Override
            public Optional<M> findOneById(@NotNull final String id)  throws Exception{
                return Optional.ofNullable(IRepository.this.findOneById(id));
            }

            @NotNull
            @Override
            public Optional<M> findOneByIndex(@NotNull final Integer index)  throws Exception{
                return Optional.ofNullable(IRepository.this.findOneByIndex(index));
            }
        };
    }

    void clear() throws Exception;

    @NotNull
    List<M> findAll() throws Exception;

    @NotNull
    List<M> findAll(@Nullable Comparator<M> comparator) throws Exception;

    @NotNull
    M add(@NotNull M model) throws Exception;

    @NotNull
    Collection<M> add(@NotNull Collection<M> models) throws Exception;

    @NotNull
    Collection<M> set(@NotNull Collection<M> models) throws Exception;

    boolean existsById(String id) throws Exception;

    @Nullable
    M findOneById(@NotNull String id) throws Exception;

    @Nullable
    M findOneByIndex(@NotNull Integer index) throws Exception;

    int getSize() throws Exception;

    @NotNull
    M remove(@NotNull M model) throws Exception;

    @Nullable
    M removeById(@NotNull String id) throws Exception;

    @Nullable
    M removeByIndex(@NotNull Integer index) throws Exception;

    void removeAll(@NotNull Collection<M> collection) throws Exception;

}
