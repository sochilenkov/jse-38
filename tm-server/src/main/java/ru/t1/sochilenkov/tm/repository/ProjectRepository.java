package ru.t1.sochilenkov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.sochilenkov.tm.enumerated.DBColumns;
import ru.t1.sochilenkov.tm.enumerated.DBTables;
import ru.t1.sochilenkov.tm.enumerated.Status;
import ru.t1.sochilenkov.tm.model.Project;
import ru.t1.sochilenkov.tm.api.repository.IProjectRepository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @Override
    @NotNull
    protected String getTableName() {
        return DBTables.TM_PROJECT.name();
    }

    @Override
    @NotNull
    public Project fetch(@NotNull final ResultSet row) throws Exception {
        @NotNull final Project project = new Project();
        project.setId(row.getString(DBColumns.ID_COLUMN.getColumnName()));
        project.setCreated(row.getTimestamp(DBColumns.CREATED_COLUMN.getColumnName()));
        project.setUserId(row.getString(DBColumns.USER_ID_COLUMN.getColumnName()));
        project.setName(row.getString(DBColumns.NAME_COLUMN.getColumnName()));
        project.setDescription(row.getString(DBColumns.DESCRIPTION_COLUMN.getColumnName()));
        project.setStatus(Status.toStatus(row.getString(DBColumns.STATUS_COLUMN.getColumnName())));
        return project;
    }

    @NotNull
    @Override
    public Project add(@NotNull final Project model) throws Exception {
        @NotNull final String query = String.format(
                "INSERT INTO %s (%s, %s, %s, %s, %s, %s) VALUES (?, ?, ?, ?, ?, ?)",
                DBTables.TM_PROJECT.name(), DBColumns.ID_COLUMN.getColumnName(),
                DBColumns.CREATED_COLUMN.getColumnName(), DBColumns.USER_ID_COLUMN.getColumnName(),
                DBColumns.NAME_COLUMN.getColumnName(), DBColumns.DESCRIPTION_COLUMN.getColumnName(),
                DBColumns.STATUS_COLUMN.getColumnName());
        try (@NotNull PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, model.getId());
            statement.setTimestamp(2, new Timestamp(model.getCreated().getTime()));
            statement.setString(3, model.getUserId());
            statement.setString(4, model.getName());
            statement.setString(5, model.getDescription());
            statement.setString(6, model.getStatus().getDisplayName());
            statement.executeUpdate();
        }
        return model;
    }

    @NotNull
    @Override
    public Project update(@NotNull Project model) throws Exception {
        @NotNull final String query = String.format(
                "UPDATE %s SET %s = ?, %s = ?, %s = ? WHERE %s = ?",
                getTableName(), DBColumns.NAME_COLUMN.getColumnName(),
                DBColumns.DESCRIPTION_COLUMN.getColumnName(), DBColumns.STATUS_COLUMN.getColumnName(),
                DBColumns.ID_COLUMN.getColumnName());
        try (@NotNull PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, model.getName());
            statement.setString(2, model.getDescription());
            statement.setString(3, model.getStatus().getDisplayName());
            statement.setString(4, model.getId());
            statement.executeUpdate();
        }
        return model;
    }

}
