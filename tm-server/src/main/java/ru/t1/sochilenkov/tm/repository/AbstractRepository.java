package ru.t1.sochilenkov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sochilenkov.tm.api.repository.IRepository;
import ru.t1.sochilenkov.tm.comparator.CreatedComparator;
import ru.t1.sochilenkov.tm.comparator.StatusComparator;
import ru.t1.sochilenkov.tm.enumerated.DBColumns;
import ru.t1.sochilenkov.tm.exception.entity.EntityNotFoundException;
import ru.t1.sochilenkov.tm.model.AbstractModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @Nullable
    protected Connection connection;

    @Override
    public void setRepositoryConnection(Connection connection) {
        this.connection = connection;
    }

    protected abstract String getTableName();

    @NotNull
    @SuppressWarnings("rawtypes")
    protected String getSortType(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return DBColumns.CREATED_COLUMN.getColumnName();
        if (comparator == StatusComparator.INSTANCE) return DBColumns.STATUS_COLUMN.getColumnName();
        else return DBColumns.NAME_COLUMN.getColumnName();
    }

    @NotNull
    public abstract M fetch(@NotNull final ResultSet row) throws Exception;

    @NotNull
    public abstract M add(@NotNull final M model) throws Exception;

    @Override
    public void clear() throws Exception {
        @NotNull final String query = String.format("TRUNCATE TABLE %s", getTableName());
        try (@NotNull final Statement statement = connection.createStatement()) {
            statement.executeUpdate(query);
        }
    }

    @NotNull
    @Override
    public List<M> findAll() throws Exception {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String query = String.format("SELECT * FROM %s", getTableName());
        try (@NotNull final Statement statement = connection.createStatement()) {
            @NotNull final ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                result.add(fetch(resultSet));
            }
        }
        return result;
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final Comparator<M> comparator) throws Exception {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String query = String.format("SELECT * FROM %s", getTableName());
        @NotNull final String order = String.format(" ORDER BY %s", getSortType(comparator));
        try (@NotNull final Statement statement = connection.createStatement()) {
            @NotNull final ResultSet resultSet = statement.executeQuery(query + order);
            while (resultSet.next()) {
                result.add(fetch(resultSet));
            }
        }
        return result;
    }

    @NotNull
    @Override
    public Collection<M> add(@NotNull Collection<M> models) throws Exception {
        @NotNull List<M> result = new ArrayList<>();
        for (M model : models) {
            result.add(add(model));
        }
        return  result;
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull Collection<M> models) throws Exception {
        clear();
        return add(models);
    }

    @Override
    public boolean existsById(final String id) throws Exception {
        return findOneById(id) != null;
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String id) throws Exception {
        @NotNull final String query = String.format("SELECT * FROM %s WHERE id = ? LIMIT 1", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, id);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }

    @Nullable
    @Override
    public M findOneByIndex(@NotNull final Integer index) throws Exception {
        @NotNull final String query = String.format("SELECT * FROM %s LIMIT ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setInt(1, index);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            for (int i = 0; i < index - 1; i++) {
                if (!resultSet.next()) return null;
            }
            return fetch(resultSet);
        }
    }

    @Override
    public int getSize() throws Exception {
        @NotNull final String query = String.format("SELECT COUNT(ID) FROM %s", getTableName());
        try (@NotNull final Statement statement = connection.createStatement()) {
            @NotNull final ResultSet resultSet = statement.executeQuery(query);
            resultSet.next();
            return resultSet.getInt("COUNT");
        }
    }

    @NotNull
    @Override
    public M remove(@NotNull final M model) throws Exception {
        @NotNull final String query = String.format("DELETE FROM %s WHERE ID = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, model.getId());
            statement.executeUpdate();
        }
        return model;
    }

    @Nullable
    @Override
    public M removeById(@NotNull final String id) throws Exception {
        @Nullable final M model = findOneById(id);
        if (model == null) throw new EntityNotFoundException();
        return remove(model);
    }

    @Nullable
    @Override
    public M removeByIndex(@NotNull final Integer index) throws Exception {
        @Nullable final M model = findOneByIndex(index);
        if (model == null) throw new EntityNotFoundException();
        return remove(model);
    }

    @Override
    public void removeAll(@NotNull final Collection<M> collection) throws Exception {
        for (M model : collection) {
            remove(model);
        }
    }

}
