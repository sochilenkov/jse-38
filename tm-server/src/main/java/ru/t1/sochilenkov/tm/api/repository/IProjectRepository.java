package ru.t1.sochilenkov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.sochilenkov.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    @NotNull
    Project update(@NotNull Project model) throws Exception;

}
