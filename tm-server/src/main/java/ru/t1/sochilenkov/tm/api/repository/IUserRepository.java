package ru.t1.sochilenkov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sochilenkov.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    User findByLogin(@NotNull String login) throws Exception;

    @Nullable
    User findByEmail(@NotNull String email) throws Exception;

    @NotNull
    Boolean isLoginExist(@NotNull String login) throws Exception;

    @NotNull
    Boolean isEmailExist(@NotNull String email) throws Exception;

    @NotNull
    User update(@NotNull User model) throws Exception;

}
