package ru.t1.sochilenkov.tm.constant;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sochilenkov.tm.enumerated.Sort;
import ru.t1.sochilenkov.tm.enumerated.Status;
import ru.t1.sochilenkov.tm.model.Task;

import java.util.Comparator;
import java.util.UUID;

public interface TaskConstant {

    int INIT_COUNT_TASKS = 5;

    @NotNull
    String USER_ID_1 = UUID.randomUUID().toString();

    @NotNull
    String USER_ID_2 = UUID.randomUUID().toString();

    @Nullable
    Task NULLABLE_TASK = null;

    @Nullable
    String NULLABLE_USER_ID = null;

    @Nullable
    String EMPTY_USER_ID = "";

    @Nullable
    String NULLABLE_TASK_ID = null;

    @NotNull
    String EMPTY_TASK_ID = "";

    @NotNull
    Sort CREATED_SORT = Sort.BY_CREATED;

    @Nullable
    Sort NULLABLE_SORT = null;

    @NotNull
    Comparator<Task> TASK_COMPARATOR = CREATED_SORT.getComparator();

    @Nullable
    Comparator<Task> NULLABLE_COMPARATOR = null;

    @Nullable
    Integer NULLABLE_INDEX = null;

    @NotNull
    Status IN_PROGRESS_STATUS = Status.IN_PROGRESS;

}
