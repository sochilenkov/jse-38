package ru.t1.sochilenkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.sochilenkov.tm.api.repository.ISessionRepository;
import ru.t1.sochilenkov.tm.api.service.IConnectionService;
import ru.t1.sochilenkov.tm.api.service.IPropertyService;
import ru.t1.sochilenkov.tm.api.service.ISessionService;
import ru.t1.sochilenkov.tm.enumerated.Role;
import ru.t1.sochilenkov.tm.exception.entity.EntityNotFoundException;
import ru.t1.sochilenkov.tm.exception.field.*;
import ru.t1.sochilenkov.tm.marker.UnitCategory;
import ru.t1.sochilenkov.tm.model.Session;
import ru.t1.sochilenkov.tm.repository.SessionRepository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static ru.t1.sochilenkov.tm.constant.SessionConstant.*;

@Category(UnitCategory.class)
public class SessionServiceTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static ISessionRepository repository = new SessionRepository();;

    @NotNull
    private static ISessionService sessionService = new SessionService(repository, connectionService);;

    @NotNull
    private List<Session> sessionList;

    @NotNull
    private List<String> userIdList = new ArrayList<>();

    @BeforeClass
    public static void changeSchema() {
        System.setProperty("database.schema", "tm-tst");
    }

    @Before
    public void init() throws Exception {
        sessionList = new ArrayList<>();
        userIdList = new ArrayList<>();
        for (int i = 0; i < INIT_COUNT_SESSIONS; i++)
            userIdList.add(UUID.randomUUID().toString());
        for (int i = 0; i < INIT_COUNT_SESSIONS; i++) {
            @NotNull final Session session = new Session();
            session.setUserId(userIdList.get(i));
            session.setRole(Role.USUAL);
            sessionService.add(session);
            sessionList.add(session);
        }
    }

    @After
    public void closeConnection() throws Exception {
        sessionService.clear();
    }

    @Test
    public void testClearWOUserId() throws Exception {
        Assert.assertEquals(INIT_COUNT_SESSIONS, sessionService.getSize());
        sessionService.clear();
        Assert.assertEquals(0, sessionService.getSize());
    }

    @Test
    public void testClearPositive() throws Exception {
        for (final String userId : userIdList) {
            Assert.assertEquals(1, sessionService.getSize(userId));
            sessionService.clear(userId);
            Assert.assertEquals(0, sessionService.getSize(userId));
        }
    }

    @Test
    public void testClearNegative() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.clear(EMPTY_USER_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.clear(NULLABLE_USER_ID));
    }

    @Test
    public void testFindAllWOUserId() throws Exception {
        @NotNull List<Session> sessions = sessionService.findAll();
        Assert.assertNotNull(sessions);
        Assert.assertEquals(sessionList.size(), sessions.size());
        for (final Session session : sessions) {
            Assert.assertEquals(sessionList.get(sessions.indexOf(session)).getId(), session.getId());
        }
    }

    @Test
    public void testFindAllNegative() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.findAll(EMPTY_USER_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.findAll(NULLABLE_USER_ID));
    }

    @Test
    public void testFindAllPositive() throws Exception {
        @NotNull List<Session> sessions;
        for (final String userId : userIdList) {
            sessions = sessionService.findAll(userId);
            Assert.assertNotNull(sessions);
            for (final Session session : sessionList) {
                if (session.getUserId().equals(userId)) {
                    Assert.assertNotNull(
                            sessions.stream()
                                    .filter(m -> session.getUserId().equals(m.getUserId()))
                                    .filter(m -> session.getId().equals(m.getId()))
                                    .findFirst()
                                    .orElse(null)
                    );
                }
            }
        }
        sessionService.clear();
        sessions = sessionService.findAll(UUID.randomUUID().toString());
        Assert.assertEquals(Collections.emptyList(), sessions);
    }

    @Test
    public void testAddSessionNegative() throws Exception {
        Assert.assertThrows(EntityNotFoundException.class, () -> sessionService.add(NULLABLE_SESSION));
        @NotNull final Session session = new Session();
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.add(NULLABLE_USER_ID, session));
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.add(EMPTY_USER_ID, session));
    }

    @Test
    public void testAddSessionPositive() throws Exception {
        Assert.assertNull(sessionService.add(userIdList.get(0), NULLABLE_SESSION));
        @NotNull Session session = new Session();
        session.setUserId(UUID.randomUUID().toString());
        session.setRole(Role.USUAL);
        Assert.assertNotNull(sessionService.add(userIdList.get(0), session));
        Assert.assertEquals(2, sessionService.getSize(userIdList.get(0)));
        session.setId(UUID.randomUUID().toString());
        Assert.assertNotNull(sessionService.add(session));
    }

    @Test
    public void testSet() throws Exception {
        Assert.assertEquals(Collections.emptyList(), sessionService.set(Collections.emptyList()));
        Assert.assertEquals(sessionList.size(), sessionService.getSize());
        sessionService.clear();
        Assert.assertEquals(0, sessionService.getSize());
        Assert.assertNotNull(sessionService.set(sessionList));
        for (int i = 0; i < sessionList.size(); i++)
            Assert.assertEquals(sessionList.get(i).getId(), sessionService.findAll().get(i).getId());
    }

    @Test
    public void testAdd() throws Exception {
        @NotNull final Session session = new Session();
        session.setUserId(UUID.randomUUID().toString());
        session.setRole(Role.USUAL);
        Assert.assertEquals(Collections.emptyList(), sessionService.add(Collections.emptyList()));
        Assert.assertEquals(sessionList.size(), sessionService.getSize());
        sessionService.clear();
        sessionService.add(session);
        Assert.assertNotNull(sessionService.add(sessionList));
        sessionList.add(0, session);
        Assert.assertEquals(sessionList.size(), sessionService.getSize());
        for (int i = 0; i < sessionList.size(); i++)
            Assert.assertEquals(sessionList.get(i).getId(), sessionService.findAll().get(i).getId());
    }

    @Test
    public void testExistsByIdNegative() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.existsById(NULLABLE_USER_ID, sessionList.get(0).getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.existsById(EMPTY_USER_ID, sessionList.get(0).getId()));
        Assert.assertThrows(IdEmptyException.class, () -> sessionService.existsById(UUID.randomUUID().toString(), NULLABLE_SESSION_ID));
        Assert.assertThrows(IdEmptyException.class, () -> sessionService.existsById(UUID.randomUUID().toString(), EMPTY_SESSION_ID));
    }

    @Test
    public void testExistsByIdPositive() throws Exception {
        Assert.assertFalse(sessionService.existsById(NULLABLE_SESSION_ID));
        Assert.assertFalse(sessionService.existsById(EMPTY_SESSION_ID));
        Assert.assertFalse(sessionService.existsById(UUID.randomUUID().toString()));
        for (final Session session : sessionList) {
            Assert.assertTrue(sessionService.existsById(session.getUserId(), session.getId()));
        }
    }

    @Test
    public void testFindOneByIdNegative() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.findOneById(NULLABLE_USER_ID, sessionList.get(0).getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.findOneById(EMPTY_USER_ID, sessionList.get(0).getId()));
        Assert.assertThrows(IdEmptyException.class, () -> sessionService.findOneById(UUID.randomUUID().toString(), NULLABLE_SESSION_ID));
        Assert.assertThrows(IdEmptyException.class, () -> sessionService.findOneById(UUID.randomUUID().toString(), EMPTY_SESSION_ID));
    }

    @Test
    public void testFindOneByIdPositive() throws Exception {
        Assert.assertNull(sessionService.findOneById(NULLABLE_SESSION_ID));
        Assert.assertNull(sessionService.findOneById(EMPTY_SESSION_ID));
        Assert.assertNull(sessionService.findOneById(UUID.randomUUID().toString()));
        for (final Session session : sessionList) {
            Assert.assertEquals(session.getId(), sessionService.findOneById(session.getUserId(), session.getId()).getId());
        }
    }

    @Test
    public void testFindOneByIndexNegative() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.findOneByIndex(NULLABLE_USER_ID, 0));
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.findOneByIndex(EMPTY_USER_ID, 0));
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionService.findOneByIndex(UUID.randomUUID().toString(), NULLABLE_INDEX));
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionService.findOneByIndex(NULLABLE_INDEX));
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionService.findOneByIndex(-1));
    }

    @Test
    public void testFindOneByIndexPositive() throws Exception {
        Assert.assertEquals(sessionList.get(0).getId(), sessionService.findOneByIndex(1).getId());
        for (final String userId : userIdList) {
            Assert.assertEquals(sessionList.get(userIdList.indexOf(userId)).getId(), sessionService.findOneByIndex(userId, 1).getId());
        }
    }

    @Test
    public void testGetSizeNegative() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.getSize(NULLABLE_USER_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.getSize(EMPTY_USER_ID));
    }

    @Test
    public void testRemoveNegative() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.remove(NULLABLE_USER_ID, sessionList.get(0)));
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.remove(EMPTY_USER_ID, sessionList.get(0)));
        Assert.assertThrows(EntityNotFoundException.class, () -> sessionService.remove(NULLABLE_SESSION));
    }

    @Test
    public void testRemoveWOUserIdPositive() throws Exception {
        for (final Session session : sessionList) {
            Assert.assertNotNull(sessionService.remove(session));
            Assert.assertFalse(sessionService.findAll().contains(session));
        }
        Assert.assertEquals(0, sessionService.getSize());
    }

    @Test
    public void testRemovePositive() throws Exception {
        Assert.assertNull(sessionService.remove(userIdList.get(0), NULLABLE_SESSION));
        for (final Session session : sessionList) {
            Assert.assertNotNull(sessionService.remove(session.getUserId(), session));
            Assert.assertFalse(sessionService.findAll(session.getUserId()).contains(session));
        }
        for (final String userId : userIdList)
            Assert.assertEquals(0, sessionService.getSize(userId));
    }

    @Test
    public void testRemoveByIdNegative() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.removeById(NULLABLE_USER_ID, NULLABLE_SESSION_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.removeById(EMPTY_USER_ID, NULLABLE_SESSION_ID));
        Assert.assertThrows(IdEmptyException.class, () -> sessionService.removeById(UUID.randomUUID().toString(), NULLABLE_SESSION_ID));
        Assert.assertThrows(IdEmptyException.class, () -> sessionService.removeById(UUID.randomUUID().toString(), EMPTY_SESSION_ID));
        Assert.assertThrows(IdEmptyException.class, () -> sessionService.removeById(NULLABLE_SESSION_ID));
        Assert.assertThrows(IdEmptyException.class, () -> sessionService.removeById(EMPTY_SESSION_ID));
    }

    @Test
    public void testRemoveByIdWOUserIdPositive() throws Exception {
        for (final Session session : sessionList) {
            Assert.assertNotNull(sessionService.removeById(session.getId()));
            Assert.assertFalse(sessionService.findAll().contains(session));
        }
        Assert.assertEquals(0, sessionService.getSize());
    }

    @Test
    public void testRemoveByIdPositive() throws Exception {
        for (final Session session : sessionList) {
            Assert.assertNotNull(sessionService.removeById(session.getUserId(), session.getId()));
            Assert.assertFalse(sessionService.findAll(session.getUserId()).contains(session));
            Assert.assertEquals(0, sessionService.getSize(session.getUserId()));
        }
    }

    @Test
    public void testRemoveByIndexNegative() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.removeByIndex(NULLABLE_USER_ID, NULLABLE_INDEX));
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.removeByIndex(EMPTY_USER_ID, NULLABLE_INDEX));
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionService.removeByIndex(UUID.randomUUID().toString(), NULLABLE_INDEX));
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionService.removeByIndex(NULLABLE_INDEX));
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionService.removeByIndex(-1));
    }

    @Test
    public void testRemoveByIndexWOUserIdPositive() throws Exception {
        for (final Session session : sessionList) {
            Assert.assertNotNull(sessionService.findOneById(session.getId()));
            Assert.assertNotNull(sessionService.removeByIndex(1));
            Assert.assertNull(sessionService.findOneById(session.getId()));
        }
        Assert.assertEquals(0, sessionService.getSize());
    }

    @Test
    public void testRemoveByIndexPositive() throws Exception {
        for (final String userId : userIdList) {
            Assert.assertEquals(1, sessionService.getSize(userId));
            Assert.assertEquals(sessionList.get(userIdList.indexOf(userId)).getId(), sessionService.removeByIndex(userId, 1).getId());
            Assert.assertEquals(0, sessionService.getSize(userId));
        }
    }

    @Test
    public void testRemoveAll() throws Exception {
        sessionService.removeAll(null);
        sessionService.removeAll(sessionList);
        Assert.assertEquals(0, sessionService.getSize());
    }

}
