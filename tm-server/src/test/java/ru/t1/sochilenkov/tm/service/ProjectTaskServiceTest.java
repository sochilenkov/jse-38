package ru.t1.sochilenkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.sochilenkov.tm.api.repository.IProjectRepository;
import ru.t1.sochilenkov.tm.api.repository.ITaskRepository;
import ru.t1.sochilenkov.tm.api.service.IConnectionService;
import ru.t1.sochilenkov.tm.api.service.IProjectTaskService;
import ru.t1.sochilenkov.tm.api.service.IPropertyService;
import ru.t1.sochilenkov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.sochilenkov.tm.exception.entity.TaskNotFoundException;
import ru.t1.sochilenkov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.sochilenkov.tm.exception.field.TaskIdEmptyException;
import ru.t1.sochilenkov.tm.exception.field.UserIdEmptyException;
import ru.t1.sochilenkov.tm.marker.UnitCategory;
import ru.t1.sochilenkov.tm.model.Project;
import ru.t1.sochilenkov.tm.model.Task;
import ru.t1.sochilenkov.tm.repository.ProjectRepository;
import ru.t1.sochilenkov.tm.repository.TaskRepository;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static ru.t1.sochilenkov.tm.constant.ProjectTaskConstant.*;

@Category(UnitCategory.class)
public class ProjectTaskServiceTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private static ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private static IProjectTaskService projectTaskService;

    @NotNull
    private List<Project> projectList;

    @NotNull
    private List<Task> taskList;

    @NotNull
    private static Connection connection;

    @BeforeClass
    public static void changeSchema() {
        System.setProperty("database.schema", "tm-tst");
    }

    @BeforeClass
    public static void initServices() throws Exception {
        projectTaskService = new ProjectTaskService(projectRepository, taskRepository, connectionService);
    }

    @Before
    public void init() throws Exception {
        projectList = new ArrayList<>();
        taskList = new ArrayList<>();
        connection = connectionService.getConnection();
        connection.setAutoCommit(true);
        projectRepository.setRepositoryConnection(connection);
        taskRepository.setRepositoryConnection(connection);
        for (int i = 1; i <= 2; i++) {
            @NotNull final Project project = new Project();
            project.setName("Project");
            project.setDescription("Description");
            project.setUserId(i == 1 ? USER_ID_1 : USER_ID_2);
            projectRepository.add(project);
            projectList.add(project);
        }
        for (int i = 1; i <= INIT_COUNT_TASKS; i++) {
            @NotNull final Task task = new Task();
            task.setName("Task_" + i);
            task.setDescription("Description_" + i);
            task.setUserId(USER_ID_1);
            taskRepository.add(task);
            taskList.add(task);
        }
        for (int i = 1; i <= INIT_COUNT_TASKS; i++) {
            @NotNull final Task task = new Task();
            task.setName("Task_" + i);
            task.setDescription("Description_" + i);
            task.setUserId(USER_ID_2);
            taskRepository.add(task);
            taskList.add(task);
        }
    }

    @After
    public void closeConnection() throws Exception {
        taskRepository.setRepositoryConnection(connection);
        projectRepository.setRepositoryConnection(connection);
        taskRepository.clear();
        projectRepository.clear();
        connection.close();
    }

    @Test
    public void testBindTaskToProjectNegative() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.bindTaskToProject(NULLABLE_USER_ID, NULLABLE_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.bindTaskToProject(EMPTY_USER_ID, NULLABLE_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.bindTaskToProject(USER_ID_1, NULLABLE_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.bindTaskToProject(USER_ID_1, EMPTY_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.bindTaskToProject(USER_ID_1, UUID.randomUUID().toString(), NULLABLE_TASK_ID));
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.bindTaskToProject(USER_ID_1, UUID.randomUUID().toString(), EMPTY_TASK_ID));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectTaskService.bindTaskToProject(UUID.randomUUID().toString(), UUID.randomUUID().toString(), UUID.randomUUID().toString()));
        taskRepository.setRepositoryConnection(connection);
        projectRepository.setRepositoryConnection(connection);
        Assert.assertThrows(TaskNotFoundException.class, () -> projectTaskService.bindTaskToProject(USER_ID_1, projectRepository.findOneByIndex(USER_ID_1, 1).getId(), UUID.randomUUID().toString()));
    }

    @Test
    public void testBindTaskToProjectPositive() throws Exception {
        for (final Project project : projectList) {
            Assert.assertEquals(Collections.emptyList(), taskRepository.findAllByProjectId(project.getUserId(), project.getId()));
            for (final Task task : taskList) {
                if (project.getUserId().equals(task.getUserId()))
                    Assert.assertNotNull(projectTaskService.bindTaskToProject(project.getUserId(), project.getId(), task.getId()));
            }
            taskRepository.setRepositoryConnection(connection);
            projectRepository.setRepositoryConnection(connection);
            List<Task> tasks = taskRepository.findAll(project.getUserId());
            for (final Task task : tasks) {
                Assert.assertNotNull(
                        taskRepository.findAllByProjectId(project.getUserId(), project.getId())
                                .stream()
                                .filter(m -> task.getUserId().equals(m.getUserId()))
                                .filter(m -> task.getId().equals(m.getId()))
                                .findFirst()
                                .orElse(null)
                );
            }
        }
    }

    @Test
    public void testUnbindTaskFromProjectNegative() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(NULLABLE_USER_ID, NULLABLE_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(EMPTY_USER_ID, NULLABLE_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(USER_ID_1, NULLABLE_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(USER_ID_1, EMPTY_PROJECT_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(USER_ID_1, UUID.randomUUID().toString(), NULLABLE_TASK_ID));
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(USER_ID_1, UUID.randomUUID().toString(), EMPTY_TASK_ID));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectTaskService.unbindTaskFromProject(UUID.randomUUID().toString(), UUID.randomUUID().toString(), UUID.randomUUID().toString()));
        taskRepository.setRepositoryConnection(connection);
        projectRepository.setRepositoryConnection(connection);
        Assert.assertThrows(TaskNotFoundException.class, () -> projectTaskService.unbindTaskFromProject(USER_ID_1, projectRepository.findOneByIndex(USER_ID_1, 1).getId(), UUID.randomUUID().toString()));
    }

    @Test
    public void TestUnbindTaskFromProjectPositive() throws Exception {
        testBindTaskToProjectPositive();
        for (final Project project : projectList) {
            Assert.assertNotEquals(Collections.emptyList(), taskRepository.findAllByProjectId(project.getUserId(), project.getId()));
            for (final Task task : taskList) {
                if (project.getUserId().equals(task.getUserId()))
                    Assert.assertNotNull(projectTaskService.unbindTaskFromProject(project.getUserId(), project.getId(), task.getId()));
            }
            taskRepository.setRepositoryConnection(connection);
            projectRepository.setRepositoryConnection(connection);
            List<Task> tasks = taskRepository.findAll(project.getUserId());
            for (final Task task : tasks) {
                Assert.assertNull(
                        taskRepository.findAllByProjectId(project.getUserId(), project.getId())
                                .stream()
                                .filter(m -> task.getUserId().equals(m.getUserId()))
                                .filter(m -> task.getId().equals(m.getId()))
                                .findFirst()
                                .orElse(null)
                );
            }
        }
    }

    @Test
    public void testRemoveProjectByIdNegative() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.removeProjectById(NULLABLE_USER_ID, NULLABLE_PROJECT_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.removeProjectById(EMPTY_USER_ID, NULLABLE_PROJECT_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.removeProjectById(USER_ID_1, NULLABLE_PROJECT_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.removeProjectById(USER_ID_1, EMPTY_PROJECT_ID));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectTaskService.removeProjectById(UUID.randomUUID().toString(), UUID.randomUUID().toString()));
    }

    @Test
    public void testRemoveProjectByIdPositive() throws Exception {
        testBindTaskToProjectPositive();
        for (final Project project : projectList) {
            taskRepository.setRepositoryConnection(connection);
            projectRepository.setRepositoryConnection(connection);
            Assert.assertNotNull(taskRepository.findAllByProjectId(project.getUserId(), project.getId()));
            Assert.assertEquals(INIT_COUNT_TASKS, taskRepository.findAll(project.getUserId()).size());
            projectTaskService.removeProjectById(project.getUserId(), project.getId());
            taskRepository.setRepositoryConnection(connection);
            projectRepository.setRepositoryConnection(connection);
            Assert.assertEquals(Collections.emptyList(), taskRepository.findAllByProjectId(project.getUserId(), project.getId()));
            Assert.assertEquals(0, taskRepository.findAll(project.getUserId()).size());
        }
        Assert.assertEquals(0, taskRepository.getSize());
    }

}
