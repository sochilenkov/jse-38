package ru.t1.sochilenkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.sochilenkov.tm.api.repository.ISessionRepository;
import ru.t1.sochilenkov.tm.api.service.IConnectionService;
import ru.t1.sochilenkov.tm.api.service.IPropertyService;
import ru.t1.sochilenkov.tm.enumerated.Role;
import ru.t1.sochilenkov.tm.exception.entity.EntityNotFoundException;
import ru.t1.sochilenkov.tm.marker.UnitCategory;
import ru.t1.sochilenkov.tm.model.Session;
import ru.t1.sochilenkov.tm.service.ConnectionService;
import ru.t1.sochilenkov.tm.service.PropertyService;

import java.sql.Connection;
import java.util.*;

import static ru.t1.sochilenkov.tm.constant.SessionConstant.*;

@Category(UnitCategory.class)
public class SessionRepositoryTest {

    @NotNull
    private ISessionRepository repository;

    @NotNull
    private List<Session> sessionList;

    @NotNull
    private List<String> userIdList = new ArrayList<>();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private Connection connection;

    @BeforeClass
    public static void changeSchema() {
        System.setProperty("database.schema", "tm-tst");
    }

    @Before
    public void init() throws Exception {
        repository = new SessionRepository();
        connection = connectionService.getConnection();
        repository.setRepositoryConnection(connection);
        connection.setAutoCommit(true);
        sessionList = new ArrayList<>();
        userIdList = new ArrayList<>();
        for (int i = 0; i < INIT_COUNT_SESSIONS; i++)
            userIdList.add(UUID.randomUUID().toString());
        for (int i = 0; i < INIT_COUNT_SESSIONS; i++) {
            @NotNull final Session session = new Session();
            session.setUserId(userIdList.get(i));
            session.setRole(Role.USUAL);
            repository.add(session);
            sessionList.add(session);
        }
    }

    @After
    public void closeConnection() throws Exception {
        repository.clear();
        connection.close();
    }

    @Test
    public void testAddSessionPositive() throws Exception {
        @NotNull Session session = new Session();
        session.setUserId(UUID.randomUUID().toString());
        session.setRole(Role.USUAL);
        Assert.assertNull(repository.add(null, session));
        Assert.assertNotNull(repository.add(userIdList.get(0), session));
    }

    @Test
    public void testClear() throws Exception {
        for (@NotNull final String userId : userIdList) {
            Assert.assertEquals(1, repository.getSize(userId));
            repository.clear(userId);
            Assert.assertEquals(0, repository.getSize(userId));
        }
    }

    @Test
    public void testClearWOUserId() throws Exception {
        Assert.assertEquals(INIT_COUNT_SESSIONS, repository.getSize());
        repository.clear();
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void testFindById() throws Exception {
        Assert.assertNull(repository.findOneById(UUID.randomUUID().toString()));
        for (@NotNull final String userId : userIdList) {
            Assert.assertNull(repository.findOneById(userId, UUID.randomUUID().toString()));
        }
        for (@NotNull final Session session : sessionList) {
            final Session foundSessionWOUser = repository.findOneById(session.getId());
            final Session foundSession = repository.findOneById(session.getUserId(), session.getId());
            Assert.assertNotNull(foundSessionWOUser);
            Assert.assertNotNull(foundSession);
            Assert.assertEquals(session.getId(), foundSessionWOUser.getId());
            Assert.assertEquals(session.getId(), foundSession.getId());
        }
    }

    @Test
    public void testExistsById() throws Exception {
        Assert.assertFalse(repository.existsById(UUID.randomUUID().toString()));
        for (@NotNull final String userId : userIdList) {
            Assert.assertFalse(repository.existsById(userId, UUID.randomUUID().toString()));
        }
        for (@NotNull final Session session : sessionList) {
            Assert.assertTrue(repository.existsById(session.getId()));
            Assert.assertTrue(repository.existsById(session.getUserId(), session.getId()));
        }
    }

    @Test
    public void testFindByIndex() throws Exception {
        Assert.assertNull(repository.findOneByIndex(9999));
        for (@NotNull final Session session : sessionList) {
            Assert.assertNull(repository.findOneByIndex(session.getUserId(), 9999));
            final Session foundSession = repository.findOneByIndex(session.getUserId(), 1);
            Assert.assertNotNull(foundSession);
            Assert.assertEquals(session.getId(), foundSession.getId());
        }
        for (@NotNull final Session session : sessionList) {
            final Session foundSessionWOUser = repository.findOneByIndex(sessionList.indexOf(session) + 1);
            Assert.assertNotNull(foundSessionWOUser);
            Assert.assertEquals(sessionList.get(sessionList.indexOf(session)).getId(), foundSessionWOUser.getId());
        }
    }

    @Test
    public void testFindAll() throws Exception {
        List<Session> sessions = repository.findAll();
        Assert.assertNotNull(sessions);
        Assert.assertEquals(sessionList.size(), sessions.size());
        for (@NotNull final Session session : sessionList) {
            Assert.assertEquals(sessions.get(sessionList.indexOf(session)).getId(), session.getId());
        }
        for (@NotNull final String userId : userIdList) {
            sessions = repository.findAll(userId);
            Assert.assertNotNull(sessions);
            Assert.assertEquals(1, sessions.size());
            for (@NotNull final Session session : sessionList) {
                if (session.getUserId().equals(userId))
                    Assert.assertNotNull(
                            sessions.stream()
                                    .filter(m -> session.getId().equals(m.getId()))
                                    .filter(m -> session.getUserId().equals(m.getUserId()))
                                    .findFirst()
                                    .orElse(null));
            }
        }
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveByIdWOUserNegative() throws Exception {
        repository.removeById(UUID.randomUUID().toString());
    }

    @Test
    public void testRemoveByIdWOUserPositive() throws Exception {
        Assert.assertEquals(INIT_COUNT_SESSIONS, repository.getSize());
        for (@NotNull final Session session : sessionList) {
            Assert.assertNotNull(repository.removeById(session.getId()));
            Assert.assertNull(repository.findOneById(session.getId()));
        }
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void testRemoveById() throws Exception {
        for (@NotNull final Session session : sessionList) {
            Assert.assertNull(repository.removeById(session.getUserId(), UUID.randomUUID().toString()));
            Assert.assertNotNull(repository.removeById(session.getUserId(), session.getId()));
            Assert.assertNull(repository.findOneById(session.getUserId(), session.getId()));
            Assert.assertEquals(0, repository.getSize(session.getUserId()));
        }
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveByIndexWOUserNegative() throws Exception {
        repository.removeByIndex(9999);
    }

    @Test
    public void testRemoveByIndexWOUserPositive() throws Exception {
        Assert.assertEquals(INIT_COUNT_SESSIONS, repository.getSize());
        for (@NotNull final Session session : sessionList) {
            Assert.assertNotNull(repository.removeByIndex(1));
            Assert.assertEquals(INIT_COUNT_SESSIONS - sessionList.indexOf(session) - 1, repository.getSize());
        }
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void testRemoveByIndex() throws Exception {
        for (@NotNull final Session session : sessionList) {
            Assert.assertEquals(1, repository.getSize(session.getUserId()));
            Assert.assertNull(repository.removeByIndex(session.getUserId(), 9999));
            Assert.assertNotNull(repository.removeByIndex(session.getUserId(), 1));
            Assert.assertEquals(0, repository.getSize(session.getUserId()));
        }
    }

    @Test
    public void testRemoveWOUserPositive() throws Exception {
        Assert.assertEquals(INIT_COUNT_SESSIONS, repository.getSize());
        for (final Session session : sessionList) {
            Assert.assertNotNull(repository.remove(session));
        }
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void testRemove() throws Exception {
        for (final Session session : sessionList) {
            Assert.assertEquals(1, repository.getSize(session.getUserId()));
            Assert.assertNotNull(repository.remove(session.getUserId(), session));
            Assert.assertEquals(0, repository.getSize(session.getUserId()));
        }
    }

    @Test
    public void testSet() throws Exception {
        List<Session> sessions = Arrays.asList(new Session(), new Session(), new Session());
        for (final Session session : sessions) {
            session.setUserId(UUID.randomUUID().toString());
            session.setRole(Role.USUAL);
        }
        Assert.assertNotNull(repository.set(sessions));
        Assert.assertEquals(3, repository.getSize());
    }

}
