package ru.t1.sochilenkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.sochilenkov.tm.api.repository.IProjectRepository;
import ru.t1.sochilenkov.tm.api.repository.ITaskRepository;
import ru.t1.sochilenkov.tm.api.repository.IUserRepository;
import ru.t1.sochilenkov.tm.api.service.IConnectionService;
import ru.t1.sochilenkov.tm.api.service.IPropertyService;
import ru.t1.sochilenkov.tm.api.service.IUserService;
import ru.t1.sochilenkov.tm.enumerated.Role;
import ru.t1.sochilenkov.tm.exception.entity.EntityNotFoundException;
import ru.t1.sochilenkov.tm.exception.entity.UserNotFoundException;
import ru.t1.sochilenkov.tm.exception.field.*;
import ru.t1.sochilenkov.tm.marker.UnitCategory;
import ru.t1.sochilenkov.tm.model.User;
import ru.t1.sochilenkov.tm.repository.ProjectRepository;
import ru.t1.sochilenkov.tm.repository.TaskRepository;
import ru.t1.sochilenkov.tm.repository.UserRepository;
import ru.t1.sochilenkov.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static ru.t1.sochilenkov.tm.constant.UserConstant.*;

@Category(UnitCategory.class)
public class UserServiceTest {

    @NotNull
    private final static IUserRepository userRepository = new UserRepository();

    @NotNull
    private final static ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final static IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final static IPropertyService propertyService = new PropertyService();

    @NotNull
    private final static IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final static IUserService userService = new UserService(userRepository, taskRepository, projectRepository, propertyService, connectionService);

    @NotNull
    private List<User> userList;

    @BeforeClass
    public static void changeSchema() {
        System.setProperty("database.schema", "tm-tst");
    }

    @Before
    public void init() throws Exception {
        userService.clear();
        userList = new ArrayList<>();
        for (int i = 1; i <= INIT_COUNT_USERS; i++) {
            @NotNull final User user = new User();
            user.setLogin("User_" + i);
            user.setEmail("User_" + i + "@test.ru");
            user.setFirstName("User_" + i);
            user.setLastName("Userovov_" + i);
            user.setLastName("Userovich_" + i);
            userService.add(user);
            userList.add(user);
        }
    }

    @After
    public void closeConnection() throws Exception {
        userService.clear();
    }

    @AfterClass
    public static void defaultUsersRestore() throws Exception {
        try {
            IUserRepository userRepository = new UserRepository();
            ITaskRepository taskRepository = new TaskRepository();
            IProjectRepository projectRepository = new ProjectRepository();
            IPropertyService propertyService = new PropertyService();
            IConnectionService connectionService = new ConnectionService(propertyService);
            IUserService userService = new UserService(userRepository, taskRepository, projectRepository, propertyService, connectionService);
            userService.create("admin", "admin", Role.ADMIN);
            userService.create("user 1", "1", "first@site.com");
            userService.create("user 2", "2", "second@site.com");
        } catch (Exception ignored) {
        }
    }

    @Test
    public void testClearPositive() throws Exception {
        Assert.assertEquals(INIT_COUNT_USERS, userService.getSize());
        userService.clear();
        Assert.assertEquals(0, userService.getSize());
    }

    @Test
    public void testFindAll() throws Exception {
        @NotNull List<User> users = userService.findAll();
        Assert.assertNotNull(users);
        Assert.assertEquals(userList.size(), users.size());
        for (final User user : users) {
            Assert.assertEquals(user.getId(), userList.get(users.indexOf(user)).getId());
        }
    }

    @Test(expected = EntityNotFoundException.class)
    public void testAddUserNegative() throws Exception {
        userService.add(NULLABLE_USER);
    }

    @Test
    public void testAddUser() throws Exception {
        Assert.assertThrows(EntityNotFoundException.class, () -> userService.add(NULLABLE_USER));
        @NotNull final User user = new User();
        Assert.assertNotNull(userService.add(user));
        Assert.assertEquals(INIT_COUNT_USERS + 1, userService.getSize());
    }

    @Test
    public void testSet() throws Exception {
        Assert.assertEquals(Collections.emptyList(), userService.set(Collections.emptyList()));
        Assert.assertEquals(userList.size(), userService.getSize());
        userService.clear();
        Assert.assertEquals(0, userService.getSize());
        Assert.assertNotNull(userService.set(userList));
        for (int i = 0; i < userList.size(); i++)
            Assert.assertEquals(userList.get(i).getId(), userService.findAll().get(i).getId());
    }

    @Test
    public void testAdd() throws Exception {
        @NotNull final User user = new User();
        Assert.assertEquals(Collections.emptyList(), userService.add(Collections.emptyList()));
        Assert.assertEquals(userList.size(), userService.getSize());
        userService.clear();
        userService.add(user);
        Assert.assertNotNull(userService.add(userList));
        userList.add(0, user);
        Assert.assertEquals(userList.size(), userService.getSize());
        for (int i = 0; i < userList.size(); i++)
            Assert.assertEquals(userList.get(i).getId(), userService.findAll().get(i).getId());
    }

    @Test
    public void testExistsById() throws Exception {
        Assert.assertFalse(userService.existsById(NULLABLE_USER_ID));
        Assert.assertFalse(userService.existsById(EMPTY_USER_ID));
        Assert.assertFalse(userService.existsById(UUID.randomUUID().toString()));
        for (final User user : userList) {
            Assert.assertTrue(userService.existsById(user.getId()));
        }
    }

    @Test
    public void testFindOneByIdPositive() throws Exception {
        Assert.assertNull(userService.findOneById(NULLABLE_USER_ID));
        Assert.assertNull(userService.findOneById(EMPTY_USER_ID));
        Assert.assertNull(userService.findOneById(UUID.randomUUID().toString()));
        for (final User user : userList) {
            Assert.assertEquals(user.getId(), userService.findOneById(user.getId()).getId());
        }
    }

    @Test
    public void testFindOneByIndexNegative() throws Exception {
        Assert.assertThrows(IndexIncorrectException.class, () -> userService.findOneByIndex(NULLABLE_INDEX));
        Assert.assertThrows(IndexIncorrectException.class, () -> userService.findOneByIndex(-1));
    }

    @Test
    public void testFindOneByIndexPositive() throws Exception {
        for (int i = 0; i < INIT_COUNT_USERS; i++) {
            Assert.assertEquals(userList.get(i).getId(), userService.findOneByIndex(i+1).getId());
        }
    }

    @Test
    public void testGetSize() throws Exception {
        Assert.assertEquals(userList.size(), userService.getSize());
    }

    @Test(expected = UserNotFoundException.class)
    public void testRemoveNegative() throws Exception {
        userService.remove(NULLABLE_USER);
    }

    @Test
    public void testRemove() throws Exception {
        for (final User user : userList) {
            Assert.assertNotNull(userService.remove(user));
            Assert.assertFalse(userService.findAll().contains(user));
        }
        Assert.assertEquals(0, userService.getSize());
    }

    @Test
    public void testRemoveByIdNegative() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> userService.removeById(NULLABLE_USER_ID));
        Assert.assertThrows(IdEmptyException.class, () -> userService.removeById(EMPTY_USER_ID));
    }

    @Test
    public void testRemoveByIdPositive() throws Exception {
        for (final User user : userList) {
            Assert.assertNotNull(userService.removeById(user.getId()));
            Assert.assertFalse(userService.findAll().contains(user));
        }
        Assert.assertEquals(0, userService.getSize());
    }

    @Test
    public void testRemoveByIndexNegative() throws Exception {
        Assert.assertThrows(IndexIncorrectException.class, () -> userService.removeByIndex(NULLABLE_INDEX));
        Assert.assertThrows(IndexIncorrectException.class, () -> userService.removeByIndex(-1));
    }

    @Test
    public void testRemoveByIndexPositive() throws Exception {
        for (final User user : userList) {
            Assert.assertNotNull(userService.findOneById(user.getId()));
            Assert.assertNotNull(userService.removeByIndex(1));
            Assert.assertNull(userService.findOneById(user.getId()));
        }
        Assert.assertEquals(0, userService.getSize());
    }

    @Test
    public void testRemoveAll() throws Exception {
        userService.removeAll(null);
        userService.removeAll(userList);
        Assert.assertEquals(0, userService.getSize());
    }

    @Test
    public void testCreateNegative() throws Exception {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create(NULLABLE_LOGIN, NULLABLE_PASSWORD));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create(EMPTY_LOGIN, NULLABLE_PASSWORD));
        Assert.assertThrows(ExistsLoginException.class, () -> userService.create(userList.get(0).getLogin(), NULLABLE_PASSWORD));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create("USR", NULLABLE_PASSWORD));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create("USR", EMPTY_PASSWORD));

        Assert.assertThrows(LoginEmptyException.class, () -> userService.create(NULLABLE_LOGIN, NULLABLE_PASSWORD, NULLABLE_EMAIL));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create(EMPTY_LOGIN, NULLABLE_PASSWORD, NULLABLE_EMAIL));
        Assert.assertThrows(ExistsLoginException.class, () -> userService.create(userList.get(0).getLogin(), NULLABLE_PASSWORD, NULLABLE_EMAIL));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create("USR", NULLABLE_PASSWORD, NULLABLE_EMAIL));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create("USR", EMPTY_PASSWORD, NULLABLE_EMAIL));
        Assert.assertThrows(ExistsEmailException.class, () -> userService.create("USR", "USR", userList.get(0).getEmail()));

        Assert.assertThrows(LoginEmptyException.class, () -> userService.create(NULLABLE_LOGIN, NULLABLE_PASSWORD, NULLABLE_ROLE));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create(EMPTY_LOGIN, NULLABLE_PASSWORD, NULLABLE_ROLE));
        Assert.assertThrows(ExistsLoginException.class, () -> userService.create(userList.get(0).getLogin(), NULLABLE_PASSWORD, NULLABLE_ROLE));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create("USR", NULLABLE_PASSWORD, NULLABLE_ROLE));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create("USR", EMPTY_PASSWORD, NULLABLE_ROLE));
        Assert.assertThrows(RoleEmptyException.class, () -> userService.create("USR", "USR", NULLABLE_ROLE));
    }

    @Test
    public void testCreatePositive() throws Exception {
        Assert.assertNotNull(userService.create("USR", "USR", NULLABLE_EMAIL));
        Assert.assertNotNull(userService.create("USR2", "USR2", "EMAIL"));
        Assert.assertNotNull(userService.create("USR3", "USR3", Role.USUAL));
        Assert.assertEquals(INIT_COUNT_USERS + 3, userService.getSize());
    }

    @Test
    public void testUpdateUserNegative() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> userService.updateUser(NULLABLE_USER_ID, null, null, null));
        Assert.assertThrows(IdEmptyException.class, () -> userService.updateUser(EMPTY_USER_ID, null, null, null));
        Assert.assertThrows(UserNotFoundException.class, () -> userService.updateUser(UUID.randomUUID().toString(), null, null, null));
    }

    @Test
    public void testUpdateUserPositive() throws Exception {
        for (final User user : userList) {
            Assert.assertNotEquals("FST", user.getFirstName());
            Assert.assertNotEquals("LST", user.getLastName());
            Assert.assertNotEquals("MID", user.getMiddleName());
            Assert.assertNotNull(userService.updateUser(user.getId(), "FST", "LST", "MID"));
            Assert.assertEquals("FST", userService.findOneById(user.getId()).getFirstName());
            Assert.assertEquals("LST", userService.findOneById(user.getId()).getLastName());
            Assert.assertEquals("MID", userService.findOneById(user.getId()).getMiddleName());
        }
    }

    @Test
    public void testSetPasswordNegative() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> userService.setPassword(NULLABLE_USER_ID, NULLABLE_PASSWORD));
        Assert.assertThrows(IdEmptyException.class, () -> userService.setPassword(EMPTY_USER_ID, NULLABLE_PASSWORD));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.setPassword(UUID.randomUUID().toString(), NULLABLE_PASSWORD));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.setPassword(UUID.randomUUID().toString(), EMPTY_PASSWORD));
        Assert.assertThrows(UserNotFoundException.class, () -> userService.setPassword(UUID.randomUUID().toString(), UUID.randomUUID().toString()));
    }

    @Test
    public void testSetPasswordPositive() throws Exception {
        for (final User user : userList) {
            Assert.assertNotNull(userService.setPassword(user.getId(), "PSW"));
            Assert.assertEquals(HashUtil.salt(propertyService, "PSW"), userService.findOneById(user.getId()).getPasswordHash());
        }
    }

    @Test
    public void testIsLoginExists() throws Exception {
        Assert.assertFalse(userService.isLoginExist(NULLABLE_LOGIN));
        Assert.assertFalse(userService.isLoginExist(EMPTY_LOGIN));
        Assert.assertTrue(userService.isLoginExist(userList.get(0).getLogin()));
    }

    @Test
    public void testIsEmailExists() throws Exception {
        Assert.assertFalse(userService.isEmailExist(NULLABLE_EMAIL));
        Assert.assertFalse(userService.isEmailExist(EMPTY_EMAIL));
        Assert.assertTrue(userService.isEmailExist(userList.get(0).getEmail()));
    }

    @Test
    public void testRemoveByLoginNegative() throws Exception {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.removeByLogin(NULLABLE_LOGIN));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.removeByLogin(EMPTY_LOGIN));
    }

    @Test
    public void testRemoveByLoginPositive() throws Exception {
        for (final User user : userList) {
            Assert.assertNotNull(userService.removeByLogin(user.getLogin()));
            Assert.assertFalse(userService.findAll().contains(user));
        }
        Assert.assertEquals(0, userService.getSize());
    }

    @Test
    public void testRemoveByEmailNegative() throws Exception {
        Assert.assertThrows(EmailEmptyException.class, () -> userService.removeByEmail(NULLABLE_EMAIL));
        Assert.assertThrows(EmailEmptyException.class, () -> userService.removeByEmail(EMPTY_EMAIL));
    }

    @Test
    public void testRemoveByEmailPositive() throws Exception {
        for (final User user : userList) {
            Assert.assertNotNull(userService.removeByEmail(user.getEmail()));
            Assert.assertFalse(userService.findAll().contains(user));
        }
        Assert.assertEquals(0, userService.getSize());
    }

    @Test
    public void testFindByEmailNegative() throws Exception {
        Assert.assertThrows(EmailEmptyException.class, () -> userService.findByEmail(NULLABLE_EMAIL));
        Assert.assertThrows(EmailEmptyException.class, () -> userService.findByEmail(EMPTY_EMAIL));
    }

    @Test
    public void testFindByEmailPositive() throws Exception {
        for (final User user : userList) {
            Assert.assertEquals(user.getId(), userService.findByEmail(user.getEmail()).getId());
        }
    }

    @Test
    public void testFindByLoginNegative() throws Exception {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.findByLogin(NULLABLE_LOGIN));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.findByLogin(EMPTY_LOGIN));
    }

    @Test
    public void testFindByLoginPositive() throws Exception {
        for (final User user : userList) {
            Assert.assertEquals(user.getId(), userService.findByLogin(user.getLogin()).getId());
        }
    }

    @Test
    public void testFindByIdNegative() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> userService.findById(NULLABLE_USER_ID));
        Assert.assertThrows(IdEmptyException.class, () -> userService.findById(EMPTY_USER_ID));
    }

    @Test
    public void testFindByIdPositive() throws Exception {
        for (final User user : userList) {
            Assert.assertEquals(user.getId(), userService.findById(user.getId()).getId());
        }
    }

    @Test
    public void testLockUserByLoginNegative() throws Exception {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.lockUserByLogin(NULLABLE_LOGIN));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.lockUserByLogin(EMPTY_LOGIN));
        Assert.assertThrows(UserNotFoundException.class, () -> userService.lockUserByLogin(UUID.randomUUID().toString()));
    }

    @Test
    public void testLockUserByLoginPositive() throws Exception {
        for (final User user : userList) {
            Assert.assertFalse(userService.findByLogin(user.getLogin()).getLocked());
            userService.lockUserByLogin(user.getLogin());
            Assert.assertTrue(userService.findByLogin(user.getLogin()).getLocked());
        }
    }

    @Test
    public void testUnlockUserByLoginNegative() throws Exception {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.unlockUserByLogin(NULLABLE_LOGIN));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.unlockUserByLogin(EMPTY_LOGIN));
        Assert.assertThrows(UserNotFoundException.class, () -> userService.unlockUserByLogin(UUID.randomUUID().toString()));
    }

    @Test
    public void testUnlockUserByLoginPositive() throws Exception {
        testLockUserByLoginPositive();
        for (final User user : userList) {
            Assert.assertTrue(userService.findByLogin(user.getLogin()).getLocked());
            userService.unlockUserByLogin(user.getLogin());
            Assert.assertFalse(userService.findByLogin(user.getLogin()).getLocked());
        }
    }

}
