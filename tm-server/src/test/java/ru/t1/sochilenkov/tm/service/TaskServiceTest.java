package ru.t1.sochilenkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.sochilenkov.tm.api.repository.ITaskRepository;
import ru.t1.sochilenkov.tm.api.service.IConnectionService;
import ru.t1.sochilenkov.tm.api.service.IPropertyService;
import ru.t1.sochilenkov.tm.api.service.ITaskService;
import ru.t1.sochilenkov.tm.exception.entity.EntityNotFoundException;
import ru.t1.sochilenkov.tm.exception.entity.TaskNotFoundException;
import ru.t1.sochilenkov.tm.exception.field.*;
import ru.t1.sochilenkov.tm.marker.UnitCategory;
import ru.t1.sochilenkov.tm.model.Project;
import ru.t1.sochilenkov.tm.model.Task;
import ru.t1.sochilenkov.tm.repository.TaskRepository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static ru.t1.sochilenkov.tm.constant.TaskConstant.*;

@Category(UnitCategory.class)
public class TaskServiceTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final ITaskRepository repository = new TaskRepository();;

    @NotNull
    private static final ITaskService taskService = new TaskService(repository, connectionService);;

    @NotNull
    private List<Task> taskList;

    @BeforeClass
    public static void changeSchema() {
        System.setProperty("database.schema", "tm-tst");
    }

    @Before
    public void init() throws Exception {
        @NotNull Project bindedProject = new Project();
        taskList = new ArrayList<>();
        for (int i = 1; i <= INIT_COUNT_TASKS; i++) {
            @NotNull final Task task = new Task();
            task.setName("Task_" + i);
            task.setDescription("Description_" + i);
            task.setUserId(USER_ID_1);
            task.setProjectId(bindedProject.getId());
            taskList.add(task);
        }
        for (int i = 1; i <= INIT_COUNT_TASKS; i++) {
            @NotNull final Task task = new Task();
            task.setName("Task_" + i);
            task.setDescription("Description_" + i);
            task.setUserId(USER_ID_2);
            taskList.add(task);
        }
        taskService.set(taskList);
    }

    @After
    public void closeConnection() throws Exception {
        taskService.clear();
    }

    @Test
    public void testClearWOUserId() throws Exception {
        Assert.assertEquals(INIT_COUNT_TASKS * 2, taskService.getSize());
        taskService.clear();
        Assert.assertEquals(0, taskService.getSize());
    }

    @Test
    public void testClearPositive() throws Exception {
        Assert.assertEquals(INIT_COUNT_TASKS, taskService.getSize(USER_ID_1));
        taskService.clear(USER_ID_1);
        Assert.assertEquals(0, taskService.getSize(USER_ID_1));
        Assert.assertEquals(INIT_COUNT_TASKS, taskService.getSize(USER_ID_2));
        taskService.clear(USER_ID_2);
        Assert.assertEquals(0, taskService.getSize(USER_ID_2));
    }

    @Test
    public void testClearNegative() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.clear(EMPTY_USER_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.clear(NULLABLE_USER_ID));
    }

    @Test
    public void testFindAllWOUserId() throws Exception {
        @NotNull List<Task> tasks = taskService.findAll();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(taskList.size(), tasks.size());
        for (final Task task : taskList) {
            Assert.assertNotNull(
                    tasks.stream()
                            .filter(m -> task.getId().equals(m.getId()))
                            .findFirst()
                            .orElse(null)
            );
        }
    }

    @Test
    public void testFindAllWOUserIdSort() throws Exception {
        @NotNull List<Task> tasks = taskService.findAll(CREATED_SORT);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(taskList.size(), tasks.size());
        for (final Task task : tasks) {
            Assert.assertNotNull(
                    tasks.stream()
                            .filter(m -> task.getId().equals(m.getId()))
                            .findFirst()
                            .orElse(null)
            );
        }
        tasks = taskService.findAll(NULLABLE_SORT);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(taskList.size(), tasks.size());
        for (final Task task : tasks) {
            Assert.assertNotNull(
                    tasks.stream()
                            .filter(m -> task.getId().equals(m.getId()))
                            .findFirst()
                            .orElse(null)
            );
        }
    }

    @Test
    public void testFindAllWOUserIdComparator() throws Exception {
        @NotNull List<Task> tasks = taskService.findAll(TASK_COMPARATOR);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(taskList.size(), tasks.size());
        for (int i = 0; i < taskList.size(); i++)
            Assert.assertEquals(taskList.get(i).getId(), taskService.findAll().get(i).getId());
        tasks = taskService.findAll(NULLABLE_COMPARATOR);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(taskList.size(), tasks.size());
        for (int i = 0; i < taskList.size(); i++)
            Assert.assertEquals(taskList.get(i).getId(), taskService.findAll().get(i).getId());
    }

    @Test
    public void testFindAllNegative() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findAll(EMPTY_USER_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findAll(NULLABLE_USER_ID));
    }

    @Test
    public void testFindAllPositive() throws Exception {
        @NotNull List<Task> tasks = taskService.findAll(USER_ID_1);
        Assert.assertNotNull(tasks);
        for (final Task task : taskList) {
            if (task.getUserId().equals(USER_ID_1))
                Assert.assertNotNull(
                        tasks.stream()
                                .filter(m -> task.getUserId().equals(m.getUserId()))
                                .filter(m -> task.getId().equals(m.getId()))
                                .findFirst()
                                .orElse(null)
                );
        }
        tasks = taskService.findAll(USER_ID_2);
        Assert.assertNotNull(tasks);
        for (final Task task : taskList) {
            if (task.getUserId().equals(USER_ID_2))
                Assert.assertNotNull(
                        tasks.stream()
                                .filter(m -> task.getUserId().equals(m.getUserId()))
                                .filter(m -> task.getId().equals(m.getId()))
                                .findFirst()
                                .orElse(null)
                );
        }
        taskService.clear();
        tasks = taskService.findAll(UUID.randomUUID().toString());
        Assert.assertEquals(Collections.emptyList(), tasks);
    }

    @Test
    public void testFindAllNegativeComparator() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findAll(NULLABLE_USER_ID, TASK_COMPARATOR));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findAll(EMPTY_USER_ID, TASK_COMPARATOR));
    }

    @Test
    public void testFindAllPositiveComparator() throws Exception {
        List<Task> tasks = taskService.findAll(USER_ID_1, TASK_COMPARATOR);
        Assert.assertNotNull(tasks);
        for (final Task task : taskList) {
            if (task.getUserId().equals(USER_ID_1))
                Assert.assertNotNull(
                        tasks.stream()
                                .filter(m -> task.getUserId().equals(m.getUserId()))
                                .filter(m -> task.getId().equals(m.getId()))
                                .findFirst()
                                .orElse(null)
                );
        }
        tasks = taskService.findAll(USER_ID_2, NULLABLE_COMPARATOR);
        Assert.assertNotNull(tasks);
        for (final Task task : taskList) {
            if (task.getUserId().equals(USER_ID_2))
                Assert.assertNotNull(
                        tasks.stream()
                                .filter(m -> task.getUserId().equals(m.getUserId()))
                                .filter(m -> task.getId().equals(m.getId()))
                                .findFirst()
                                .orElse(null)
                );
        }
    }

    @Test
    public void testFindAllSortNegative() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findAll(NULLABLE_USER_ID, CREATED_SORT));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findAll(EMPTY_USER_ID, CREATED_SORT));
    }

    @Test
    public void testFindAllSortPositive() throws Exception {
        List<Task> tasks = taskService.findAll(USER_ID_1, CREATED_SORT);
        Assert.assertNotNull(tasks);
        for (final Task task : taskList) {
            if (task.getUserId().equals(USER_ID_1))
                Assert.assertNotNull(
                        tasks.stream()
                                .filter(m -> task.getUserId().equals(m.getUserId()))
                                .filter(m -> task.getId().equals(m.getId()))
                                .findFirst()
                                .orElse(null)
                );
        }
        tasks = taskService.findAll(USER_ID_2, NULLABLE_SORT);
        Assert.assertNotNull(tasks);
        for (final Task task : taskList) {
            if (task.getUserId().equals(USER_ID_2))
                Assert.assertNotNull(
                        tasks.stream()
                                .filter(m -> task.getUserId().equals(m.getUserId()))
                                .filter(m -> task.getId().equals(m.getId()))
                                .findFirst()
                                .orElse(null)
                );
        }
    }

    @Test
    public void testAddTaskNegative() throws Exception {
        Assert.assertThrows(EntityNotFoundException.class, () -> taskService.add(NULLABLE_TASK));
        @NotNull final Task task = new Task();
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.add(NULLABLE_USER_ID, task));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.add(EMPTY_USER_ID, task));
    }

    @Test
    public void testAddTaskPositive() throws Exception {
        Assert.assertNull(taskService.add(USER_ID_1, NULLABLE_TASK));
        @NotNull Task task = new Task();
        task.setName("TaskAddTest");
        task.setDescription("TaskAddTest desc");
        Assert.assertNotNull(taskService.add(USER_ID_1, task));
        Assert.assertEquals(INIT_COUNT_TASKS + 1, taskService.getSize(USER_ID_1));
        task.setId(UUID.randomUUID().toString());
        Assert.assertNotNull(taskService.add(task));
    }

    @Test
    public void testSet() throws Exception {
        Assert.assertEquals(Collections.emptyList(), taskService.set(Collections.emptyList()));
        Assert.assertEquals(taskList.size(), taskService.getSize());
        taskService.clear();
        Assert.assertEquals(0, taskService.getSize());
        Assert.assertNotNull(taskService.set(taskList));
        for (int i = 0; i < taskList.size(); i++)
            Assert.assertEquals(taskList.get(i).getId(), taskService.findAll().get(i).getId());
    }

    @Test
    public void testAdd() throws Exception {
        @NotNull final Task task = new Task();
        Assert.assertEquals(Collections.emptyList(), taskService.add(Collections.emptyList()));
        Assert.assertEquals(taskList.size(), taskService.getSize());
        taskService.clear();
        taskService.add(task);
        Assert.assertNotNull(taskService.add(taskList));
        taskList.add(0, task);
        Assert.assertEquals(taskList.size(), taskService.getSize());
        for (int i = 0; i < taskList.size(); i++)
            Assert.assertEquals(taskList.get(i).getId(), taskService.findAll().get(i).getId());
    }

    @Test
    public void testExistsByIdNegative() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.existsById(NULLABLE_USER_ID, taskList.get(0).getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.existsById(EMPTY_USER_ID, taskList.get(0).getId()));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.existsById(USER_ID_1, NULLABLE_TASK_ID));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.existsById(USER_ID_1, EMPTY_TASK_ID));
    }

    @Test
    public void testExistsByIdPositive() throws Exception {
        Assert.assertFalse(taskService.existsById(NULLABLE_TASK_ID));
        Assert.assertFalse(taskService.existsById(EMPTY_TASK_ID));
        Assert.assertFalse(taskService.existsById(UUID.randomUUID().toString()));
        for (final Task task : taskList) {
            Assert.assertTrue(taskService.existsById(task.getUserId(), task.getId()));
        }
    }

    @Test
    public void testFindOneByIdNegative() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findOneById(NULLABLE_USER_ID, taskList.get(0).getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findOneById(EMPTY_USER_ID, taskList.get(0).getId()));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.findOneById(USER_ID_1, NULLABLE_TASK_ID));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.findOneById(USER_ID_1, EMPTY_TASK_ID));
    }

    @Test
    public void testFindOneByIdPositive() throws Exception {
        Assert.assertNull(taskService.findOneById(NULLABLE_TASK_ID));
        Assert.assertNull(taskService.findOneById(EMPTY_TASK_ID));
        Assert.assertNull(taskService.findOneById(UUID.randomUUID().toString()));
        for (final Task task : taskList) {
            Assert.assertEquals(task.getId(), taskService.findOneById(task.getUserId(), task.getId()).getId());
        }
    }

    @Test
    public void testFindOneByIndexNegative() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findOneByIndex(NULLABLE_USER_ID, 0));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findOneByIndex(EMPTY_USER_ID, 0));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.findOneByIndex(USER_ID_1, NULLABLE_INDEX));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.findOneByIndex(NULLABLE_INDEX));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.findOneByIndex(-1));
    }

    @Test
    public void testFindOneByIndexPositive() throws Exception {
        Assert.assertEquals(taskList.get(0).getId(), taskService.findOneByIndex(1).getId());
        for (int i = 0; i < INIT_COUNT_TASKS; i++) {
            Assert.assertEquals(taskList.get(i).getId(), taskService.findOneByIndex(USER_ID_1, i+1).getId());
        }
        for (int i = 5; i < INIT_COUNT_TASKS * 2; i++) {
            Assert.assertEquals(taskList.get(i).getId(), taskService.findOneByIndex(USER_ID_2, i - 4).getId());
        }
    }

    @Test
    public void testGetSizeNegative() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.getSize(NULLABLE_USER_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.getSize(EMPTY_USER_ID));
    }

    @Test
    public void testRemoveNegative() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.remove(NULLABLE_USER_ID, taskList.get(0)));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.remove(EMPTY_USER_ID, taskList.get(0)));
        Assert.assertThrows(EntityNotFoundException.class, () -> taskService.remove(NULLABLE_TASK));
    }

    @Test
    public void testRemoveWOUserIdPositive() throws Exception {
        for (final Task task : taskList) {
            Assert.assertNotNull(taskService.remove(task));
            Assert.assertFalse(taskService.findAll().contains(task));
        }
        Assert.assertEquals(0, taskService.getSize());
    }

    @Test
    public void testRemovePositive() throws Exception {
        Assert.assertNull(taskService.remove(USER_ID_1, NULLABLE_TASK));
        for (final Task task : taskList) {
            Assert.assertNotNull(taskService.remove(task.getUserId(), task));
            Assert.assertFalse(taskService.findAll(task.getUserId()).contains(task));
        }
        Assert.assertEquals(0, taskService.getSize(USER_ID_1));
        Assert.assertEquals(0, taskService.getSize(USER_ID_2));
    }

    @Test
    public void testRemoveByIdNegative() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.removeById(NULLABLE_USER_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.removeById(EMPTY_USER_ID, NULLABLE_TASK_ID));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.removeById(USER_ID_1, NULLABLE_TASK_ID));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.removeById(USER_ID_1, EMPTY_TASK_ID));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.removeById(NULLABLE_TASK_ID));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.removeById(EMPTY_TASK_ID));
    }

    @Test
    public void testRemoveByIdWOUserIdPositive() throws Exception {
        for (final Task task : taskList) {
            Assert.assertNotNull(taskService.removeById(task.getId()));
            Assert.assertFalse(taskService.findAll().contains(task));
        }
        Assert.assertEquals(0, taskService.getSize());
    }

    @Test
    public void testRemoveByIdPositive() throws Exception {
        for (final Task task : taskList) {
            Assert.assertNotNull(taskService.removeById(task.getUserId(), task.getId()));
            Assert.assertFalse(taskService.findAll(task.getUserId()).contains(task));
        }
        Assert.assertEquals(0, taskService.getSize(USER_ID_1));
        Assert.assertEquals(0, taskService.getSize(USER_ID_2));
    }

    @Test
    public void testRemoveByIndexNegative() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.removeByIndex(NULLABLE_USER_ID, NULLABLE_INDEX));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.removeByIndex(EMPTY_USER_ID, NULLABLE_INDEX));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.removeByIndex(USER_ID_1, NULLABLE_INDEX));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.removeByIndex(NULLABLE_INDEX));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.removeByIndex(-1));
    }

    @Test
    public void testRemoveByIndexWOUserIdPositive() throws Exception {
        for (final Task task : taskList) {
            Assert.assertNotNull(taskService.removeByIndex(1));
        }
        Assert.assertEquals(0, taskService.getSize());
    }

    @Test
    public void testRemoveByIndexPositive() throws Exception {
        Assert.assertEquals(INIT_COUNT_TASKS, taskService.getSize(USER_ID_1));
        for (int i = 0; i < INIT_COUNT_TASKS; i++) {
            Assert.assertEquals(taskList.get(i).getId(), taskService.removeByIndex(USER_ID_1, 1).getId());
        }
        Assert.assertEquals(INIT_COUNT_TASKS, taskService.getSize(USER_ID_2));
        for (int i = 5; i < INIT_COUNT_TASKS * 2; i++) {
            Assert.assertEquals(taskList.get(i).getId(), taskService.removeByIndex(USER_ID_2, 1).getId());
        }
        Assert.assertEquals(0, taskService.getSize(USER_ID_1));
        Assert.assertEquals(0, taskService.getSize(USER_ID_2));
    }

    @Test
    public void testRemoveAll() throws Exception {
        taskService.removeAll(null);
        taskService.removeAll(taskList);
        Assert.assertEquals(0, taskService.getSize());
    }

    @Test
    public void testCreateTaskNegative() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.create(NULLABLE_USER_ID, null, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.create(EMPTY_USER_ID, null, null));
        Assert.assertThrows(NameEmptyException.class, () -> taskService.create(USER_ID_1, null, null));
        Assert.assertThrows(NameEmptyException.class, () -> taskService.create(USER_ID_1, "", null));
    }

    @Test
    public void testCreateTaskPositive() throws Exception {
        Assert.assertEquals(INIT_COUNT_TASKS, taskService.getSize(USER_ID_1));
        Assert.assertNotNull(taskService.create(USER_ID_1, "PROJ", "PROJ_DESC"));
        Assert.assertEquals(INIT_COUNT_TASKS + 1, taskService.getSize(USER_ID_1));

        Assert.assertEquals(INIT_COUNT_TASKS, taskService.getSize(USER_ID_2));
        Assert.assertNotNull(taskService.create(USER_ID_2, "PROJ", "PROJ_DESC"));
        Assert.assertEquals(INIT_COUNT_TASKS + 1, taskService.getSize(USER_ID_2));

        Assert.assertEquals(INIT_COUNT_TASKS + 1, taskService.getSize(USER_ID_1));
        Assert.assertNotNull(taskService.create(USER_ID_1, "PROJ_2", ""));
        Assert.assertEquals(INIT_COUNT_TASKS + 2, taskService.getSize(USER_ID_1));

        Assert.assertEquals(INIT_COUNT_TASKS + 1, taskService.getSize(USER_ID_2));
        Assert.assertNotNull(taskService.create(USER_ID_2, "PROJ_2", ""));
        Assert.assertEquals(INIT_COUNT_TASKS + 2, taskService.getSize(USER_ID_2));

        Assert.assertEquals(INIT_COUNT_TASKS + 2, taskService.getSize(USER_ID_1));
        Assert.assertNotNull(taskService.create(USER_ID_1, "PROJ_3", null));
        Assert.assertEquals(INIT_COUNT_TASKS + 3, taskService.getSize(USER_ID_1));
    }

    @Test
    public void testChangeTaskStatusByIdNegative() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.changeTaskStatusById(NULLABLE_USER_ID, null, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.changeTaskStatusById(EMPTY_USER_ID, null, null));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.changeTaskStatusById(USER_ID_1, null, null));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.changeTaskStatusById(USER_ID_1, "", null));
        Assert.assertThrows(TaskNotFoundException.class, () -> taskService.changeTaskStatusById(USER_ID_1, UUID.randomUUID().toString(), null));
        Assert.assertThrows(StatusIncorrectException.class, () -> taskService.changeTaskStatusById(USER_ID_1, taskService.findOneByIndex(USER_ID_1, 1).getId(), null));
    }

    @Test
    public void testChangeTaskStatusByIdPositive() throws Exception {
        for (final Task task : taskList) {
            Assert.assertNotNull(taskService.changeTaskStatusById(task.getUserId(), task.getId(), IN_PROGRESS_STATUS));
            Assert.assertEquals(IN_PROGRESS_STATUS, taskService.findOneById(task.getUserId(), task.getId()).getStatus());
        }
    }

    @Test
    public void testChangeTaskStatusByIndexNegative() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.changeTaskStatusByIndex(NULLABLE_USER_ID, NULLABLE_INDEX, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.changeTaskStatusByIndex(EMPTY_USER_ID, NULLABLE_INDEX, null));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.changeTaskStatusByIndex(USER_ID_1, NULLABLE_INDEX, null));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.changeTaskStatusByIndex(USER_ID_1, -1, null));
        Assert.assertThrows(TaskNotFoundException.class, () -> taskService.changeTaskStatusByIndex(UUID.randomUUID().toString(), 0, null));
        Assert.assertThrows(StatusIncorrectException.class, () -> taskService.changeTaskStatusByIndex(USER_ID_1, 1, null));
    }

    @Test
    public void testChangeTaskStatusByIndexPositive() throws Exception {
        @Nullable Task task;
        for (int i = 0; i < INIT_COUNT_TASKS; i++) {
            task = taskService.changeTaskStatusByIndex(USER_ID_1, i + 1, IN_PROGRESS_STATUS);
            Assert.assertNotNull(task);
            Assert.assertEquals(IN_PROGRESS_STATUS, taskService.findOneById(USER_ID_1, task.getId()).getStatus());
        }
        for (int i = 5; i < INIT_COUNT_TASKS * 2; i++) {
            task = taskService.changeTaskStatusByIndex(USER_ID_2, i - 4, IN_PROGRESS_STATUS);
            Assert.assertNotNull(task);
            Assert.assertEquals(IN_PROGRESS_STATUS, taskService.findOneById(USER_ID_2, task.getId()).getStatus());
        }
    }

    @Test
    public void testUpdateByIdNegative() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.updateById(NULLABLE_USER_ID, null, null, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.updateById(EMPTY_USER_ID, null, null, null));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.updateById(USER_ID_1, null, null, null));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.updateById(USER_ID_1, "", null, null));
        Assert.assertThrows(NameEmptyException.class, () -> taskService.updateById(USER_ID_1, UUID.randomUUID().toString(), null, null));
        Assert.assertThrows(NameEmptyException.class, () -> taskService.updateById(USER_ID_1, UUID.randomUUID().toString(), "", null));
        Assert.assertThrows(TaskNotFoundException.class, () -> taskService.updateById(USER_ID_1, UUID.randomUUID().toString(), "UPD_PROJ_1", null));
    }

    @Test
    public void testUpdateByIdPositive() throws Exception {
        for (final Task task : taskList) {
            Assert.assertNotNull(taskService.updateById(task.getUserId(), task.getId(), task.getName(), null));
            Assert.assertNotNull(taskService.updateById(task.getUserId(), task.getId(), task.getName(), ""));
            Assert.assertNotNull(taskService.updateById(task.getUserId(), task.getId(), task.getName() + "_upd", task.getDescription() + "_upd"));
            Assert.assertEquals(task.getId(), taskService.findOneById(task.getUserId(), task.getId()).getId());
        }
    }

    @Test
    public void testUpdateByIndexNegative() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.updateByIndex(NULLABLE_USER_ID, null, null, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.updateByIndex(EMPTY_USER_ID, null, null, null));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.updateByIndex(USER_ID_1, null, null, null));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.updateByIndex(USER_ID_1, -1, null, null));
        Assert.assertThrows(NameEmptyException.class, () -> taskService.updateByIndex(USER_ID_1, 0, null, null));
        Assert.assertThrows(NameEmptyException.class, () -> taskService.updateByIndex(USER_ID_1, 0, "", null));
        Assert.assertThrows(TaskNotFoundException.class, () -> taskService.updateByIndex(UUID.randomUUID().toString(), 0, "UPD_PROJ_1", null));
    }

    @Test
    public void testUpdateByIndexPositive() throws Exception {
        for (int i = 0; i < INIT_COUNT_TASKS; i++) {
            Task task = taskList.get(i);
            Assert.assertNotNull(taskService.updateByIndex(task.getUserId(), i + 1, task.getName(), null));
            Assert.assertNotNull(taskService.updateByIndex(task.getUserId(), i + 1, task.getName(), ""));
            Assert.assertNotNull(taskService.updateByIndex(task.getUserId(), i + 1, task.getName() + "_upd", task.getDescription() + "_upd"));
            task.setName(task.getName() + "_upd");
            Assert.assertNotNull(
                    taskService.findAll().stream()
                            .filter(m -> task.getUserId().equals(m.getUserId()))
                            .filter(m -> task.getName().equals(m.getName()))
                            .findFirst()
                            .orElse(null)
            );
        }
        for (int i = 5; i < INIT_COUNT_TASKS * 2; i++) {
            Task task = taskList.get(i);
            Assert.assertNotNull(taskService.updateByIndex(task.getUserId(), i - 4, task.getName(), null));
            Assert.assertNotNull(taskService.updateByIndex(task.getUserId(), i - 4, task.getName(), ""));
            Assert.assertNotNull(taskService.updateByIndex(task.getUserId(), i - 4, task.getName() + "_upd", task.getDescription() + "_upd"));
            task.setName(task.getName() + "_upd");
            Assert.assertNotNull(
                    taskService.findAll().stream()
                            .filter(m -> task.getUserId().equals(m.getUserId()))
                            .filter(m -> task.getName().equals(m.getName()))
                            .findFirst()
                            .orElse(null)
            );
        }
    }

    @Test
    public void testFindAllByProjectIdNegative() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findAllByProjectId(NULLABLE_USER_ID, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findAllByProjectId(EMPTY_USER_ID, null));
    }

    @Test
    public void testFindAllByProjectIdPositive() throws Exception {
        Assert.assertEquals(Collections.emptyList(), taskService.findAllByProjectId(USER_ID_1, null));
        Assert.assertEquals(Collections.emptyList(), taskService.findAllByProjectId(USER_ID_1, ""));
        for (final Task task : taskList) {
            if (task.getProjectId() != null)
                Assert.assertNotNull(
                        taskService.findAllByProjectId(task.getUserId(), task.getProjectId())
                                .stream()
                                .filter(m -> task.getUserId().equals(m.getUserId()))
                                .filter(m -> task.getId().equals(m.getId()))
                                .findFirst()
                                .orElse(null)
                );
        }
    }

}
