package ru.t1.sochilenkov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sochilenkov.tm.dto.request.ProjectListRequest;
import ru.t1.sochilenkov.tm.enumerated.Sort;
import ru.t1.sochilenkov.tm.model.Project;
import ru.t1.sochilenkov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    @NotNull
    public static final String DESCRIPTION = "Show all projects.";

    @NotNull
    public static final String NAME = "project-list";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @Nullable final Sort sort = Sort.toSort(sortType);

        @NotNull final ProjectListRequest request = new ProjectListRequest(getToken());
        request.setSort(sort);

        @Nullable final List<Project> projects = getProjectEndpoint().listProject(request).getProjects();
        int index = 1;
        if (projects != null) {
            for (@Nullable final Project project : projects) {
                if (project == null) continue;
                System.out.println(index + ". " + project.getName() + " | Status: " + project.getStatus().getDisplayName() + " |");
                index++;
            }
        }
    }

}
